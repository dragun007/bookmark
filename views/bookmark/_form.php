<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bookmark */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookmark-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'url')->textarea(['rows' => 6]) ?>
	<?= $form->field($model, '_password')->passwordInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

