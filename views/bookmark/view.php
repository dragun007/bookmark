<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bookmark */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Bookmarks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bookmark-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
	<?php
    if($model->password!==NULL)
    {
	Modal::begin([
		'header' => '<h2>Удаление закладки</h2>',
		'toggleButton' => [
			'label' => 'Delete',
			'tag' => 'button',
			'class' => 'btn btn-danger',
		],
	]);
	?>
	<?php $form = ActiveForm::begin(['action' =>['delete','id'=>$model->id], 'method' => 'post']); ?>
	<?= $form->field($model, '_password')->passwordInput() ?>
    <div class="form-group">
		<?= Html::submitButton('delete', ['class' => 'btn btn-delete']) ?>
    </div>
	<?php ActiveForm::end();

	Modal::end();
    }
	?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'created_at',
            'favicon:ntext',
            'url:ntext',
            'title:ntext',
            'description:ntext',
            'keywords:ntext',
        ],
    ]) ?>
    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
