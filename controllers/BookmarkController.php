<?php

namespace app\controllers;

use Yii;
use app\models\Bookmark;
use app\models\BookmarkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * BookmarkController implements the CRUD actions for Bookmark model.
 */
class BookmarkController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}

	/**
	 * Lists all Bookmark models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new BookmarkSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}

	/**
	 * Отдает файл экселя со списком всех закладок и удаляет его.
	 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
	 */
	public function actionGetExcel() {

		$path = Yii::getAlias( '@runtime' ) . '/' . Bookmark::EXCEL_FILE_NAME;
		Bookmark::createExcel( $path );

		$filename = htmlspecialchars_decode( basename( $path ) );
		$size     = filesize( $path );
		header( "Content-Disposition: attachment; filename=\"$filename\"" );
		header( "Content-Length: $size" );
		header( "Content-Type: application/octet-stream" );
		if ( @readfile( $path ) ) {
			unlink( $path );
		}

	}

	/**
	 * Displays a single Bookmark model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}

	/**
	 * Creates a new Bookmark model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Bookmark();

		if ( $model->load( Yii::$app->request->post() ) &&$model->validate()) {

			if ( $model->compose() ) {
				if ( empty( $model->_password ) ) {
					$model->password = null;
				} else {
					$model->password = Yii::$app->getSecurity()->generatePasswordHash( $model->_password );
				}
				$model->save();
				return $this->redirect( [ 'view', 'id' => $model->id ] );
			}

		}

		return $this->render( 'create', [
			'model' => $model,
		] );
	}

	/**
	 * Deletes an existing Bookmark model if password right.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @param string $_password
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id) {
		$model = $this->findModel( $id );
		if($model->load( Yii::$app->request->post())&&$model->validate())
		{
			if ( Yii::$app->getSecurity()->validatePassword( $model->_password, $model->password ) ) {
				$model->delete();
				return $this->redirect( [ 'index' ] );
			} else {
				Yii::$app->session->setFlash( 'error', "Неверный пароль" );


			}
		}
		return $this->redirect( [ 'view', 'id'=>$id ] );


	}

	/**
	 * Finds the Bookmark model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Bookmark the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Bookmark::findOne( $id ) ) !== null ) {
			return $model;
		}

		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
