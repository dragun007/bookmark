<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * This is the model class for table "bookmark".
 *
 * @property int $id
 * @property string|null $favicon
 * @property string|null $url
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property timestamp $created_at
 * @property string|null $password
 */
class Bookmark extends \yii\db\ActiveRecord
{


	/**
	 * @var string
	 */
	public $_password;

	/**
	 * @var \keltstr\simplehtmldom\simple_html_dom
	 */
	private $dom;
	/**
	 *
	 */
	const SCENARIO_ADD_PAGE='add_page';
	/**
	 * название файла в который записываеться xlsx
	 */
	const EXCEL_FILE_NAME='file.xlsx';

	/**
	 * {@inheritdoc}
	 */

	public function beforeSave($insert)
	{
		if($insert)
			$this->created_at=new Expression('NOW()');
		return parent::beforeSave($insert);
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bookmark';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['favicon', 'title', 'description', 'keywords'], 'string','on' => self::SCENARIO_ADD_PAGE],
	        [['favicon'],'url', 'defaultScheme' => 'http','on' => self::SCENARIO_ADD_PAGE],
	        [['url','password','_password'], 'string'],
	        [['url'],'url', 'defaultScheme' => 'http'],
	        ['url','unique'],
	        ['url','required']
        ];
    }

	/**
	 * создает файл эксля в пути $path
	 * @param string $path
	 *
	 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
	 */
	public static function createExcel($path)
    {
	    $spreadsheet = new Spreadsheet();
	    $sheet = $spreadsheet->getActiveSheet();
	    $bookmarks=self::find()->all();
	    $row=1;


	    foreach ($bookmarks as $item)
	    {
		    $column=1;
		    //создаем загаловок
	    	if($row==1)
		    {
			    foreach ($item->attributes() as $attribute)
			    {
				    $sheet->setCellValueByColumnAndRow($column++, $row, $attribute);
			    }
			    $row++;
			    $column=1;
		    }
			// заполняем таблицу
		    foreach ($item->getAttributes() as $attribute)
		    {
			    $sheet->setCellValueByColumnAndRow($column++, $row, $attribute);
		    }
		    $row++;
	    }
	    $writer = new Xlsx($spreadsheet);
	    $writer->save($path);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'favicon' => 'Favicon',
            'url' => 'URL страницы',
            'title' => 'Заголовок страницы',
            'description' => 'META Description',
            'keywords' => 'META Keywords',
	        'created_at' => 'Дата добавления',
	        'password' => 'Пароль',
            '_password' => 'Пароль'

        ];
    }

	/**
	 * @param $selector
	 * @param $attribute
	 * @param $inner
	 */
	private  function setDomElem($selector,$attribute,$inner)
	{
		$element=$this->dom -> find($selector ,0);
		if($element!==NULL)
		{
			try{
				$this->$attribute=$element->$inner;
			}
			catch (\Exception $e) {

			}
		}
	}
	/**
	 * Собирает и выставляет все атрибуты, возвращает true в случае успеха
	 * @return bool
	 */
	public function compose()
    {
	    $this->dom =$this-> request($this->url);
	    $return=false;
		if($this->dom!==false)
		{
			$this->setDomElem('title','title','innertext');
			$this->setDomElem('meta[name=description]','description','content');
			$this->setDomElem('meta[name=keywords]','keywords','content');
			$this->setDomElem('link[rel=icon]','favicon','href');
			$return=true;
		}
		return $return;

    }

	/**
	 * Делает запрос к $url и возвращает объект dom в случае успеха или false
	 * @param $url
	 *
	 * @return bool|\keltstr\simplehtmldom\simple_html_dom
	 */
	private function request ($url) {
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	    $str = curl_exec($curl); // Retrieving the page as a string
	    curl_close($curl);
	    $return=false;
	    if($str === false)
	    {
		    $this->addError('url','Ошибка curl: ' . curl_error($curl));
	    }
		else
		{
			try {
				$return = \keltstr\simplehtmldom\SimpleHTMLDom::str_get_html($str); // Translating the string to an object
			}
			catch (\Exception $e)
			{
				$this->addError('url','Ошибка парсинга: ' . $e);
				//throw new \Exception("Ошибка парсинга",0,$e); //более читабельное исключение,
				// но не будет формы.
			}
		}



	    return $return;
    }
}
