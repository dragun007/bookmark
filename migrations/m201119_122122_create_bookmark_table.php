<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bookmark}}`.
 */
class m201119_122122_create_bookmark_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bookmark}}', [
            'id' => $this->primaryKey(),
	        'favicon' => $this->text(),
            'url' => $this->text(),
            'title' => $this->text(),
            'description' => $this->text(),
            'keywords' => $this->text(),
            'created_at' => $this->timestamp(),
            'password' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bookmark}}');
    }
}
